package org.iniad.se.sample13;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class MainActivityTest {
    private MainActivity activity;

    @Before
    public void seUp() {
        activity = new MainActivity();
    }

    @Test
    public void testMakeResult1() {
        String x = "1.0";
        String y = "2.0";
        assertThat(activity.makeResult(x, y, R.id.radioButtonAddition), is("1.0 + 2.0 = 3.0"));
    }

    @Test
    public void testMakeResult2() {
        String x = "1.0";
        String y = "2.0";
        assertThat(activity.makeResult(x, y, R.id.radioButtonSubstraction), is("1.0 - 2.0 = -1.0"));
    }

    @Test
    public void testMakeResult3() {
        String x = "1.0";
        String y = "2.0";
        assertThat(activity.makeResult(x, y,-1), is("Invalid operation."));
    }

    @Test
    public void testMakeResult4() {
        String x = "1.0";
        String y = "x";
        assertThat(activity.makeResult(x, y, R.id.radioButtonAddition), is("Invalid format."));
    }

    @Test
    public void testMakeResult5() {
        String x = "1.0";
        String y = "2.0";
        assertThat(activity.makeResult(x, y, R.id.radioButtonMultiplication), is("1.0 * 2.0 = 2.0"));
    }

    @Test
    public void testMakeResult6() {
        String x = "2.0";
        String y = "1.0";
        assertThat(activity.makeResult(x, y, R.id.radioButtonDivision), is("2.0 / 1.0 = 2.0"));
    }
}